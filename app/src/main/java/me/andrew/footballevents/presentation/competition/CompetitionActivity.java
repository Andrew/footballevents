package me.andrew.footballevents.presentation.competition;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import me.andrew.footballevents.Const;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.annotation.Layout;
import me.andrew.footballevents.common.architecture.base.BaseActivity;
import me.andrew.footballevents.common.util.FragmentUtils;

@Layout(resId = R.layout.activity_fragment_container, titleResId = R.string.title_competition)
public class CompetitionActivity extends BaseActivity {
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		int id = getIntent().getIntExtra(Const.EXTRA_COMPETITION_ID, 0);

		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragment_container);
		if (fragment == null) {
			fragment = CompetitionFragment.newInstance(id);
			FragmentUtils.replaceFragment(fm, fragment, R.id.fragment_container);
		}
	}
}
