package me.andrew.footballevents.presentation.leaguetable.recycler;


import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.andrew.footballevents.App;
import me.andrew.footballevents.common.architecture.basemvp.BaseMvpPresenter;
import me.andrew.footballevents.common.concurrency.AppExecutors;
import me.andrew.footballevents.common.error.ErrorHandler;
import me.andrew.footballevents.data.Repository;

public class LeagueTablePresenter extends BaseMvpPresenter<LeagueTableContract.View>
		implements LeagueTableContract.Presenter {
	@Inject Repository mRepository;
	@Inject AppExecutors mAppExecutors;

	public LeagueTablePresenter(LeagueTableContract.View view) {
		super(view);
		App.getComponent().inject(this);
	}

	@Override
	public void start() {
		super.start();
		loadItemList(1, true);
	}

	@Override
	public void loadItemList(int page, boolean newList) {
		if (!mView.isDataShown()) {
			mView.setRefreshing(true);
		}

		int id = mView.getCompetitionId();
		Disposable disposable = mRepository.getLeagueTable(id)
				.subscribeOn(Schedulers.from(mAppExecutors.networkIO()))
				.observeOn(Schedulers.from(mAppExecutors.mainThread()))
				.subscribe(
						response -> {
							mView.setRefreshing(false);
							mView.setLeagueCaption(response.leagueCaption);
							mView.updateItemList(response.standing, newList);
						},
						error -> {
							mView.setRefreshing(false);
							mView.setEmptyViewVisible();
							ErrorHandler.caughtException(error);
							mView.showError(error);
						});

		addDisposable(disposable);
	}
}
