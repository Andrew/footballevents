package me.andrew.footballevents.presentation.seasons.recycler;


import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseAdapter;
import me.andrew.footballevents.data.entity.Season;


public class SeasonsAdapter extends BaseAdapter<Season, SeasonsAdapter.ViewHolder> {
	SeasonsAdapter() {
		super(R.layout.item_season);
	}

	@Override
	public ViewHolder createViewHolder(View v) {
		return new ViewHolder(v);
	}

	class ViewHolder extends BaseAdapter.ViewHolder<Season, BaseAdapter.OnItemClickListener> {
		@BindView(R.id.text_caption) TextView mTextCaption;
		@BindView(R.id.text_league) TextView mTextLeague;
		@BindView(R.id.text_year) TextView mTextYear;

		ViewHolder(View itemView) {
			super(itemView);
		}

		@Override
		public void bind(BaseAdapter.OnItemClickListener listener, Season item, int pos) {
			itemView.setOnClickListener(v -> {
				if (listener != null) {
					listener.onItemClick(item, pos);
				}
			});

			mTextCaption.setText(item.caption);
			mTextLeague.setText(item.league);
			mTextYear.setText(item.year);
		}
	}
}
