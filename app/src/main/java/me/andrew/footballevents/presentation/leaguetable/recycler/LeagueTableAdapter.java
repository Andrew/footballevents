package me.andrew.footballevents.presentation.leaguetable.recycler;


import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseAdapter;
import me.andrew.footballevents.data.entity.Standing;


public class LeagueTableAdapter extends BaseAdapter<Standing, LeagueTableAdapter.ViewHolder> {
	LeagueTableAdapter() {
		super(R.layout.item_league_table);
	}

	@Override
	public ViewHolder createViewHolder(View v) {
		return new ViewHolder(v);
	}

	class ViewHolder extends BaseAdapter.ViewHolder<Standing, BaseAdapter.OnItemClickListener> {
		@BindView(R.id.text_team_name) TextView mTextTeamName;
		@BindView(R.id.text_position) TextView mTextPosition;
		@BindView(R.id.text_wins) TextView mTextWins;

		ViewHolder(View itemView) {
			super(itemView);
		}

		@Override
		public void bind(OnItemClickListener listener, Standing item, int pos) {
			itemView.setOnClickListener(v -> {
				if (listener != null) {
					listener.onItemClick(item, pos);					
				}
			});
			
			mTextTeamName.setText(item.teamName);
			mTextPosition.setText(String.valueOf(item.position));
			mTextWins.setText(item.wins + "/" + item.draws + "/" + item.losses);
		}
	}
}
