package me.andrew.footballevents.presentation.leaguetable.recycler;


import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseRecyclerContract;
import me.andrew.footballevents.data.entity.Standing;

public interface LeagueTableContract {
	interface View extends BaseRecyclerContract.View<Standing, Presenter> {
		int getCompetitionId();
		
		void setLeagueCaption(String leagueCaption);
	}

	interface Presenter extends BaseRecyclerContract.Presenter {

	}
}
