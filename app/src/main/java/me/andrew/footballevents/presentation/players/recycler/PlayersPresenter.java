package me.andrew.footballevents.presentation.players.recycler;


import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.andrew.footballevents.App;
import me.andrew.footballevents.common.architecture.basemvp.BaseMvpPresenter;
import me.andrew.footballevents.common.concurrency.AppExecutors;
import me.andrew.footballevents.common.error.ErrorHandler;
import me.andrew.footballevents.data.Repository;

public class PlayersPresenter extends BaseMvpPresenter<PlayersContract.View>
		implements PlayersContract.Presenter {
	@Inject Repository mRepository;
	@Inject AppExecutors mAppExecutors;

	public PlayersPresenter(PlayersContract.View view) {
		super(view);
		App.getComponent().inject(this);
	}

	@Override
	public void start() {
		super.start();
		loadTeamDetails();
		loadItemList(1, true);
	}

	@Override
	public void loadItemList(int page, boolean newList) {
		if (!mView.isDataShown()) {
			mView.setRefreshing(true);
		}

		String teamId = mView.getTeamId();
		Disposable disposable = mRepository.getPlayers(teamId)
				.subscribeOn(Schedulers.from(mAppExecutors.networkIO()))
				.observeOn(Schedulers.from(mAppExecutors.mainThread()))
				.subscribe(
						response -> {
							mView.setRefreshing(false);
							mView.updateItemList(response.players, newList);
							mView.sortItems((o1, o2) -> o1.position.compareTo(o2.position));
						},
						error -> {
							mView.setRefreshing(false);
							mView.setEmptyViewVisible();
							ErrorHandler.caughtException(error);
							mView.showError(error);
						});

		addDisposable(disposable);
	}

	@Override
	public void loadTeamDetails() {
		String teamId = mView.getTeamId();
		Disposable disposable = mRepository.getTeam(teamId)
				.subscribeOn(Schedulers.from(mAppExecutors.networkIO()))
				.observeOn(Schedulers.from(mAppExecutors.mainThread()))
				.subscribe(
						response -> mView.setTeamName(response.name),
						error -> {
							ErrorHandler.caughtException(error);
							mView.showError(error);
						});

		addDisposable(disposable);
	}
}
