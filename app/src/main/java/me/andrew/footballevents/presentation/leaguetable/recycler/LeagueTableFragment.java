package me.andrew.footballevents.presentation.leaguetable.recycler;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.andrew.footballevents.Const;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseAdapter;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseRecyclerFragment;
import me.andrew.footballevents.data.entity.Standing;
import me.andrew.footballevents.presentation.players.PlayersActivity;

public class LeagueTableFragment extends BaseRecyclerFragment<Standing, LeagueTableContract.Presenter>
		implements LeagueTableContract.View {

	@BindView(R.id.text_league_caption) TextView mTextLeagueCaption;

	private int mId;

	public static LeagueTableFragment newInstance(int id) {
		Bundle args = new Bundle();
		args.putInt(Const.EXTRA_COMPETITION_ID, id);
		LeagueTableFragment fragment = new LeagueTableFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public LeagueTableContract.Presenter createPresenter() {
		return new LeagueTablePresenter(this);
	}

	@Override
	public int getCompetitionId() {
		return mId;
	}

	@Override
	public void setLeagueCaption(String leagueCaption) {
		mTextLeagueCaption.setText(leagueCaption);
	}

	@Override
	protected BaseAdapter createAdapter() {
		return new LeagueTableAdapter();
	}

	@Override
	protected int getLayoutResId() {
		return R.layout.fragment_league_table;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
			savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		ButterKnife.bind(this, view);

		mId = getArguments().getInt(Const.EXTRA_COMPETITION_ID, 0);

		mAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<Standing>() {
			@Override
			public void onItemClick(Standing standing, int position) {
				Context context = getContext();
				Intent intent = new Intent(context, PlayersActivity.class);
				
				String teamLink = standing.links.teamLink.href;
				String teamId = teamLink.substring(teamLink.lastIndexOf("/") + 1);
				
				intent.putExtra(Const.EXTRA_TEAM_ID, teamId);
				context.startActivity(intent);
			}
		});

		return view;
	}
}
