package me.andrew.footballevents.presentation.players.recycler;


import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseAdapter;
import me.andrew.footballevents.data.entity.Player;


public class PlayersAdapter extends BaseAdapter<Player, PlayersAdapter.ViewHolder> {
	PlayersAdapter() {
		super(R.layout.item_player);
	}

	@Override
	public ViewHolder createViewHolder(View v) {
		return new ViewHolder(v);
	}

	class ViewHolder extends BaseAdapter.ViewHolder<Player, BaseAdapter.OnItemClickListener> {
		@BindView(R.id.text_player) TextView mTextPlayer;

		ViewHolder(View itemView) {
			super(itemView);
		}

		@Override
		public void bind(OnItemClickListener listener, Player item, int pos) {
			itemView.setOnClickListener(v -> {
				if (listener != null) {
					listener.onItemClick(item, pos);					
				}
			});

			mTextPlayer.setText(item.position + " - " + item.name);
		}
	}
}
