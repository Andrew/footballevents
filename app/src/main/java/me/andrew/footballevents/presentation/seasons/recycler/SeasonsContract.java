package me.andrew.footballevents.presentation.seasons.recycler;


import me.andrew.footballevents.data.entity.Season;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseRecyclerContract;

public interface SeasonsContract {
	interface View extends BaseRecyclerContract.View<Season, Presenter> {

	}

	interface Presenter extends BaseRecyclerContract.Presenter {

	}
}
