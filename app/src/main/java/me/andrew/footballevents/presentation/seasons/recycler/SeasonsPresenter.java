package me.andrew.footballevents.presentation.seasons.recycler;


import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.andrew.footballevents.App;
import me.andrew.footballevents.common.architecture.basemvp.BaseMvpPresenter;
import me.andrew.footballevents.common.concurrency.AppExecutors;
import me.andrew.footballevents.common.error.ErrorHandler;
import me.andrew.footballevents.data.Repository;

public class SeasonsPresenter extends BaseMvpPresenter<SeasonsContract.View>
		implements SeasonsContract.Presenter {
	@Inject Repository mRepository;
	@Inject AppExecutors mAppExecutors;

	public SeasonsPresenter(SeasonsContract.View view) {
		super(view);
		App.getComponent().inject(this);
	}

	@Override
	public void start() {
		super.start();
		loadItemList(1, true);
	}

	@Override
	public void loadItemList(int page, boolean newList) {
		if (!mView.isDataShown()) {
			mView.setRefreshing(true);
		}

		Disposable disposable = mRepository.getSeasonList()
				.subscribeOn(Schedulers.from(mAppExecutors.networkIO()))
				.observeOn(Schedulers.from(mAppExecutors.mainThread()))
				.subscribe(
						response -> {
							mView.setRefreshing(false);
							mView.updateItemList(response, newList);
						},
						error -> {
							mView.setRefreshing(false);
							mView.setEmptyViewVisible();
							ErrorHandler.caughtException(error);
							mView.showError(error);
						});

		addDisposable(disposable);
	}
}
