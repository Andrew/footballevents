package me.andrew.footballevents.presentation.competition;


import me.andrew.footballevents.common.architecture.basemvp.BaseMvpContract;
import me.andrew.footballevents.data.entity.Competition;

public interface CompetitionContract {
	interface View extends BaseMvpContract.View<Presenter> {
		int getCompetitionId();

		void showCompetitionDetails(Competition competition);
	}

	interface Presenter extends BaseMvpContract.Presenter {
		void loadCompetitionDetails();
	}
}
