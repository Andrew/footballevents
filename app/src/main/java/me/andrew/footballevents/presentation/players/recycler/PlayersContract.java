package me.andrew.footballevents.presentation.players.recycler;


import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseRecyclerContract;
import me.andrew.footballevents.data.entity.Player;

public interface PlayersContract {
	interface View extends BaseRecyclerContract.View<Player, Presenter> {
		String getTeamId();
		
		void setTeamName(String teamName);
	}

	interface Presenter extends BaseRecyclerContract.Presenter {
		void loadTeamDetails();
	}
}
