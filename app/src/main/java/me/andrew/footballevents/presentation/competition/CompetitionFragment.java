package me.andrew.footballevents.presentation.competition;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.andrew.footballevents.Const;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.BaseMvpFragment;
import me.andrew.footballevents.data.entity.Competition;
import me.andrew.footballevents.presentation.leaguetable.LeagueTableActivity;

public class CompetitionFragment extends BaseMvpFragment<CompetitionContract.Presenter>
		implements CompetitionContract.View {
	@BindView(R.id.text_caption) TextView mTextCaption;
	@BindView(R.id.text_league) TextView mTextLeague;
	@BindView(R.id.text_year) TextView mTextYear;
	@BindView(R.id.button_league_table) Button mButtonLeagueTable;
	
	private int mId;
	
	public static CompetitionFragment newInstance(int id) {
		Bundle args = new Bundle();
		args.putInt(Const.EXTRA_COMPETITION_ID, id);
		CompetitionFragment fragment = new CompetitionFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle 
			savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_competition, container, false);
		ButterKnife.bind(this, view);
		
		mId = getArguments().getInt(Const.EXTRA_COMPETITION_ID, 0);

		mButtonLeagueTable.setOnClickListener(v -> {
			Context context = getContext();
			Intent intent = new Intent(context, LeagueTableActivity.class);
			intent.putExtra(Const.EXTRA_COMPETITION_ID, mId);
			context.startActivity(intent);
		});
		
		return view;
	}

	@Override
	public CompetitionContract.Presenter createPresenter() {
		return new CompetitionPresenter(this);
	}

	@Override
	public int getCompetitionId() {
		return mId;
	}

	@Override
	public void showCompetitionDetails(Competition competition) {
		mTextCaption.setText(competition.caption);
		mTextLeague.setText(competition.league);
		mTextYear.setText(competition.year);
	}
}
