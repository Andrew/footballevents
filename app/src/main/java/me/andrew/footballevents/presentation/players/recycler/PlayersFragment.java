package me.andrew.footballevents.presentation.players.recycler;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.andrew.footballevents.Const;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseAdapter;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseRecyclerFragment;
import me.andrew.footballevents.data.entity.Player;

public class PlayersFragment extends BaseRecyclerFragment<Player, PlayersContract.Presenter>
		implements PlayersContract.View {

	@BindView(R.id.text_team_name) TextView mTextTeamName;

	private String mTeamId;

	public static PlayersFragment newInstance(String teamId) {
		Bundle args = new Bundle();
		args.putString(Const.EXTRA_TEAM_ID, teamId);
		PlayersFragment fragment = new PlayersFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public PlayersContract.Presenter createPresenter() {
		return new PlayersPresenter(this);
	}

	@Override
	public String getTeamId() {
		return mTeamId;
	}

	@Override
	public void setTeamName(String teamName) {
		mTextTeamName.setText(teamName);
	}

	@Override
	protected BaseAdapter createAdapter() {
		return new PlayersAdapter();
	}

	@Override
	protected int getLayoutResId() {
		return R.layout.fragment_players;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
			savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		ButterKnife.bind(this, view);

		mTeamId = getArguments().getString(Const.EXTRA_TEAM_ID);
		
//		Almost all teams are empty. So to check behaviour mTeamId = "445" is used 
 		mTeamId = "445";

		return view;
	}
}
