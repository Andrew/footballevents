package me.andrew.footballevents.presentation.competition;


import javax.inject.Inject;

import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import me.andrew.footballevents.App;
import me.andrew.footballevents.common.architecture.basemvp.BaseMvpPresenter;
import me.andrew.footballevents.common.concurrency.AppExecutors;
import me.andrew.footballevents.common.error.ErrorHandler;
import me.andrew.footballevents.data.Repository;

public class CompetitionPresenter extends BaseMvpPresenter<CompetitionContract.View>
		implements CompetitionContract.Presenter {
	@Inject Repository mRepository;
	@Inject AppExecutors mAppExecutors;
	
	public CompetitionPresenter(CompetitionContract.View view) {
		super(view);
		App.getComponent().inject(this);
	}

	@Override
	public void start() {
		super.start();
		loadCompetitionDetails();
	}

	@Override
	public void loadCompetitionDetails() {
		int id = mView.getCompetitionId();
		Disposable disposable = mRepository.getCompetition(id)
				.subscribeOn(Schedulers.from(mAppExecutors.networkIO()))
				.observeOn(Schedulers.from(mAppExecutors.mainThread()))
				.subscribe(
						response -> mView.showCompetitionDetails(response),
						error -> {
							ErrorHandler.caughtException(error);
							mView.showError(error);
						});

		addDisposable(disposable);
	}
}
