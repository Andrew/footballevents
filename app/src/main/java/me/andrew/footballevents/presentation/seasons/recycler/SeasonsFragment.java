package me.andrew.footballevents.presentation.seasons.recycler;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import me.andrew.footballevents.Const;
import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseAdapter;
import me.andrew.footballevents.common.architecture.basemvp.recycler.BaseRecyclerFragment;
import me.andrew.footballevents.data.entity.Season;
import me.andrew.footballevents.presentation.competition.CompetitionActivity;

public class SeasonsFragment extends BaseRecyclerFragment<Season, SeasonsContract.Presenter>
		implements SeasonsContract.View {

	public static SeasonsFragment newInstance() {
		return new SeasonsFragment();
	}

	@Override
	public SeasonsContract.Presenter createPresenter() {
		return new SeasonsPresenter(this);
	}

	@Override
	protected BaseAdapter createAdapter() {
		return new SeasonsAdapter();
	}

	@Override
	protected int getLayoutResId() {
		return R.layout.fragment_seasons;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mAdapter.setOnItemClickListener(new BaseAdapter.OnItemClickListener<Season>() {
			@Override
			public void onItemClick(Season season, int position) {
				Context context = getContext();
				Intent intent = new Intent(context, CompetitionActivity.class);
				intent.putExtra(Const.EXTRA_COMPETITION_ID, season.id);
				context.startActivity(intent);
			}
		});
	}
}
