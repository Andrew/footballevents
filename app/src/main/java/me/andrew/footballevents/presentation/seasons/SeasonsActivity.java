package me.andrew.footballevents.presentation.seasons;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.annotation.Layout;
import me.andrew.footballevents.common.architecture.base.BaseActivity;
import me.andrew.footballevents.common.util.FragmentUtils;
import me.andrew.footballevents.presentation.seasons.recycler.SeasonsFragment;

@Layout(resId = R.layout.activity_fragment_container, titleResId = R.string.title_seasons, showHomeButton = false)
public class SeasonsActivity extends BaseActivity {
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		FragmentManager fm = getSupportFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.fragment_container);
		if (fragment == null) {
			fragment = SeasonsFragment.newInstance();
			FragmentUtils.replaceFragment(fm, fragment, R.id.fragment_container);
		}
	}
}
