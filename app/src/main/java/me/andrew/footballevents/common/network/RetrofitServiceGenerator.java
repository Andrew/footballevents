package me.andrew.footballevents.common.network;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitServiceGenerator {
	private static final String HTTP_LOG = "HTTP_LOG";
	private static final int CACHE_SIZE = 10 * 1024 * 1024;

	public static <T> T createService(Class<T> clazz, String baseUrl) {
		return createService((File) null, clazz, baseUrl, new ArrayList<>());
	}

	public static <T> T createService(File cacheDir, Class<T> clazz, String baseUrl) {
		return createService(cacheDir, clazz, baseUrl, new ArrayList<>());
	}

	public static <T> T createService(Context context, boolean withCache, Class<T> clazz, String baseUrl) {
		File cacheDir = null;
		if (withCache) {
			cacheDir = context.getApplicationContext().getCacheDir();
		}
		return createService(cacheDir, clazz, baseUrl, new ArrayList<>());
	}

	public static <T> T createService(@Nullable File cacheDir, Class<T> clazz,
									  String baseUrl, List<Interceptor> interceptors) {
		HttpLoggingInterceptor.Logger logger = message -> Log.d(HTTP_LOG, message);

		HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(logger)
				.setLevel(HttpLoggingInterceptor.Level.BODY);

		OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder()
				.addInterceptor(loggingInterceptor);

		for (Interceptor i : interceptors) {
			okHttpBuilder.addInterceptor(i);
		}

		if (cacheDir != null) {
			Cache cache = new Cache(cacheDir, CACHE_SIZE);
			okHttpBuilder.cache(cache);
		}

		Retrofit retrofit = new Retrofit.Builder()
				.client(okHttpBuilder.build())
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.baseUrl(baseUrl)
				.build();

		return retrofit.create(clazz);
	}
}

