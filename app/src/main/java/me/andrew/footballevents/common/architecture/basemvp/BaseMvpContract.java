package me.andrew.footballevents.common.architecture.basemvp;


import io.reactivex.disposables.Disposable;

public interface BaseMvpContract {
	interface View<P extends Presenter> {
		P createPresenter();
		
		void showError(Throwable t);
	}
	
	interface Presenter {
		void start();

		void stop();

		void addDisposable(Disposable disposable);
	}
}
