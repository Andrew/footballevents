package me.andrew.footballevents.common.util;


import android.content.Context;
import android.support.annotation.AttrRes;
import android.util.TypedValue;

public class AttributeUtils {
	public static int getColor(Context context, @AttrRes int attrResId) {
		TypedValue value = new TypedValue();
		context.getTheme().resolveAttribute(attrResId, value, true);
		return value.data;
	}
}
