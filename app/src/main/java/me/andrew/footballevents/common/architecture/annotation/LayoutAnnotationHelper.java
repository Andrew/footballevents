package me.andrew.footballevents.common.architecture.annotation;


import java.lang.annotation.Annotation;

public class LayoutAnnotationHelper {
	private int mResId = -1;
	private int mTitleResId = -1;
	private boolean mShowHomeButton = true;

	public LayoutAnnotationHelper(Class<?> clazz) {
		if (clazz.isAnnotationPresent(Layout.class)) {
			Annotation annotation = clazz.getAnnotation(Layout.class);
			Layout layout = (Layout) annotation;
			mResId = layout.resId();
			mTitleResId = layout.titleResId();
			mShowHomeButton = layout.showHomeButton();
		}

		if (mResId == -1) {
			throw new UnsupportedOperationException("Layout resource id should be set!");
		}
	}
	
	public int getResId() {
		return mResId;
	}

	public int getTitleResId() {
		return mTitleResId;
	}

	public boolean shouldShowHomeButton() {
		return mShowHomeButton;
	}
}
