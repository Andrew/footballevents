package me.andrew.footballevents.common.architecture.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Layout {
	int resId() default -1;
	int titleResId() default -1;
	boolean showHomeButton() default true;
}
