package me.andrew.footballevents.common.architecture.basemvp.recycler;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Comparator;
import java.util.List;

import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.basemvp.BaseMvpFragment;
import me.andrew.footballevents.common.util.AttributeUtils;

/**
 * Because Recycler Fragment's logic is usually the same i.e showItems, hideItems, showRefreshing etc.
 * We can reuse this logic.
 * 
 * @param <T>
 * @param <P>
 */
public abstract class BaseRecyclerFragment<T, P extends BaseRecyclerContract.Presenter>
		extends BaseMvpFragment<P> implements BaseRecyclerContract.View<T, P> {
	protected BaseAdapter mAdapter;
	private RecyclerView mRecycler;
	private SwipeRefreshLayout mRefresh;
	private View mViewEmpty;
	private boolean mDataShown;

	protected abstract BaseAdapter createAdapter();
	protected abstract int getLayoutResId();

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mAdapter = createAdapter();
	}

	@Override
	public void onResume() {
		super.onResume();

		if (mDataShown) {
			setRecyclerVisible();
		} else {
			setEmptyViewVisible();
		}
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle
			savedInstanceState) {
		View view = inflater.inflate(getLayoutResId(), container, false);

		mRecycler = view.findViewById(R.id.recycler_view);
		mRefresh = view.findViewById(R.id.swipe_refresh);
		mViewEmpty = view.findViewById(R.id.empty_view);

		LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
		mRecycler.setLayoutManager(layoutManager);
		mRecycler.setAdapter(mAdapter);

		mRefresh.setColorSchemeColors(AttributeUtils.getColor(getActivity(), R.attr.colorAccent));
		mRefresh.setOnRefreshListener(() -> {
			mRefresh.setRefreshing(false);
			mPresenter.loadItemList(1, true);
		});

		return view;
	}

	@Override
	public boolean isDataShown() {
		return mDataShown;
	}

	@Override
	public void updateItemList(List<T> items, boolean newList) {
		if ((items == null) || (items.size() == 0)) {
			if (newList) {
				setEmptyViewVisible();
			}
		} else if (newList) {
			setItems(items);
		} else {
			addItems(items);
		}
	}
	
	@Override
	public void setItems(@NonNull List<T> items) {
		mAdapter.setItems(items);
		mDataShown = true;
		setRecyclerVisible();
	}

	@Override
	public void addItems(List<T> items) {
		mAdapter.addItems(items);
		mDataShown = true;
		setRecyclerVisible();
	}

	@Override
	public void addItem(T item) {
		mAdapter.addItem(item);
		mDataShown = true;
		setRecyclerVisible();
	}

	@Override
	public void insertItem(int position, T item) {
		mAdapter.insertItem(position, item);
		mDataShown = true;
		setRecyclerVisible();
		mRecycler.smoothScrollToPosition(position);
	}

	/**
	 * To use only if item's position is unknown
	 *
	 * @param item
	 */
	@Override
	public void removeItem(T item) {
		int position = mAdapter.getItemPosition(item);
		mAdapter.removeItem(position);
	}

	@Override
	public void removeItem(int position) {
		mAdapter.removeItem(position);
	}

	@Override
	public void sortItems(Comparator<T> comparator) {
		mAdapter.sortItems(comparator);
	}

	@Override
	public void setEmptyViewVisible() {
		mRecycler.setVisibility(View.GONE);

		if (mViewEmpty != null) {
			mViewEmpty.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void setRecyclerVisible() {
		mRecycler.setVisibility(View.VISIBLE);

		if (mViewEmpty != null) {
			mViewEmpty.setVisibility(View.GONE);
		}
	}

	@Override
	public void setRefreshing(boolean refreshing) {
		mRefresh.setRefreshing(refreshing);
	}
}
