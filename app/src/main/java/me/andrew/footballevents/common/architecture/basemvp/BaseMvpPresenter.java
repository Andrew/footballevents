package me.andrew.footballevents.common.architecture.basemvp;


import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class BaseMvpPresenter<V extends BaseMvpContract.View> implements BaseMvpContract.Presenter {
	protected V mView;
	private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
	
	public BaseMvpPresenter(V view) {
		mView = view;
	}

	@Override
	public void start() {
	}

	@Override
	public void stop() {
		mCompositeDisposable.clear();
	}

	@Override
	public void addDisposable(Disposable disposable) {
		mCompositeDisposable.add(disposable);
	}
}
