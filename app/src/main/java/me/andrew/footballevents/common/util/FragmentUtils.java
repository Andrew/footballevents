package me.andrew.footballevents.common.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FragmentUtils {
	public static void replaceFragment(FragmentManager fm, Fragment fragment, int containerId) {
		replaceFragment(fm, fragment, containerId, false, 0, 0);
	}

	public static void replaceFragment(FragmentManager fm, Fragment fragment, int containerId,
									   boolean addToBackStack) {

		replaceFragment(fm, fragment, containerId, addToBackStack, 0, 0);
	}

	public static void replaceFragment(FragmentManager fm, Fragment fragment, int containerId,
									   boolean addToBackStack,
									   int enterAnimation, int leaveAnimation) {

		FragmentTransaction ft = fm.beginTransaction();
		ft.setCustomAnimations(enterAnimation, leaveAnimation);
		ft.replace(containerId, fragment);

		if (addToBackStack) {
			ft.addToBackStack(null);
		}

		ft.commit();
	}

	public static void removeFragment(FragmentManager fm, Fragment fragment) {
		FragmentTransaction ft = fm.beginTransaction();
		ft.remove(fragment);
		ft.commit();
	}
}
