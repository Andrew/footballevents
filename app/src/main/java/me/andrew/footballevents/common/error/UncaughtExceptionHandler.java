package me.andrew.footballevents.common.error;


public class UncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
	@Override
	public void uncaughtException(Thread t, Throwable e) {
		ErrorHandler.uncaughtException(e);
	}
}
