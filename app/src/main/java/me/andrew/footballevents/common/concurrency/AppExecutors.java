package me.andrew.footballevents.common.concurrency;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AppExecutors {

	private static final int THREAD_COUNT = 3;

	private final Executor mDiskIO;
	private final Executor mNetworkIO;
	private final Executor mMainThread;

	public AppExecutors() {
		mDiskIO = Executors.newSingleThreadExecutor();
		mNetworkIO = Executors.newFixedThreadPool(THREAD_COUNT);
		mMainThread = new MainThreadExecutor();
	}

	public Executor networkIO() {
		return mNetworkIO;
	}

	public Executor diskIO() {
		return mDiskIO;
	}

	public Executor mainThread() {
		return mMainThread;
	}

	private static class MainThreadExecutor implements Executor {
		private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

		@Override
		public void execute(@NonNull Runnable command) {
			mainThreadHandler.post(command);
		}
	}
}
