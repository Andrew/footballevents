package me.andrew.footballevents.common.architecture.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import me.andrew.footballevents.R;
import me.andrew.footballevents.common.architecture.annotation.LayoutAnnotationHelper;

public abstract class BaseActivity extends AppCompatActivity {
	protected Toolbar mToolbar;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LayoutAnnotationHelper annotationHelper = new LayoutAnnotationHelper(this.getClass());
		setContentView(annotationHelper.getResId());

		mToolbar = findViewById(R.id.toolbar);

		setupToolbar(annotationHelper.shouldShowHomeButton());
		setToolbarTitle(annotationHelper.getTitleResId());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void setToolbarTitle(int title) {
		if (mToolbar != null) {
			ActionBar actionBar = getSupportActionBar();
			if (actionBar != null) {
				actionBar.setTitle(title);
			}
		}
	}

	protected void setupToolbar(boolean showHomeButton) {
		if (mToolbar != null) {
			setSupportActionBar(mToolbar);
			ActionBar actionBar = getSupportActionBar();
			if (actionBar != null) {
				actionBar.setDisplayHomeAsUpEnabled(showHomeButton);
				actionBar.setTitle("");
			}
		}
	}
}
