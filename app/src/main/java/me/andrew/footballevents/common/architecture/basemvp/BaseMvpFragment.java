package me.andrew.footballevents.common.architecture.basemvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import me.andrew.footballevents.common.error.ErrorAlertDialog;


public abstract class BaseMvpFragment<P extends BaseMvpContract.Presenter> extends Fragment
		implements BaseMvpContract.View<P> {

	protected P mPresenter;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = createPresenter();
	}

	@Override
	public void onStart() {
		super.onStart();
		mPresenter.start();
	}

	@Override
	public void onStop() {
		mPresenter.stop();
		super.onStop();
	}

	@Override
	public void showError(Throwable t) {
		ErrorAlertDialog.show(getContext(), t);
	}
}
