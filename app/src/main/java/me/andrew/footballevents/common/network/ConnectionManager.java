package me.andrew.footballevents.common.network;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;

import me.andrew.footballevents.App;


public class ConnectionManager {
	private static ConnectionManager INSTANCE;
	@Inject Context mContext;
	private boolean mOnline;

	private ConnectionManager() {
		App.getComponent().inject(this);
		mOnline = isConnected(mContext);
		registerConnectionReceiver(mContext);
	}

	public static ConnectionManager getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ConnectionManager();
		}
		return INSTANCE;
	}

	public boolean isOnline() {
		return mOnline;
	}

	private boolean isConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return (netInfo != null) && (netInfo.isConnected());
	}

	private void registerConnectionReceiver(Context context) {
		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		context.registerReceiver(new ConnectionReceiver(), filter);
	}

	private class ConnectionReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			mOnline = isConnected(context);
		}
	}
}
