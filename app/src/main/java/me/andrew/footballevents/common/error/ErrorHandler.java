package me.andrew.footballevents.common.error;


import android.util.Log;

import me.andrew.footballevents.BuildConfig;
import me.andrew.footballevents.common.network.ConnectionManager;

public class ErrorHandler {
	private static final String ERROR_LOG = "ERROR_LOG";
	private static final String CAUGHT_EXCEPTION = "CAUGHT EXCEPTION";
	private static final String UNCAUGHT_EXCEPTION = "UNCAUGHT EXCEPTION";
	
	public static void caughtException(Throwable t) {
		if (!BuildConfig.DEBUG && ConnectionManager.getInstance().isOnline()) {
			// send crash report
		}

		Log.e(ERROR_LOG, CAUGHT_EXCEPTION, t);
	}

	static void uncaughtException(Throwable t) {
		if (!BuildConfig.DEBUG && ConnectionManager.getInstance().isOnline()) {
			// send crash report
		}

		Log.e(ERROR_LOG, UNCAUGHT_EXCEPTION, t);
	}
}
