package me.andrew.footballevents.common.architecture.basemvp.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;

public abstract class BaseAdapter<T, VH extends BaseAdapter.ViewHolder> extends RecyclerView.Adapter<VH> {
	protected List<T> mItems;
	private OnItemClickListener<T> mListener;
	private int mLayoutId;

	public abstract VH createViewHolder(View v);

	public BaseAdapter(int layoutId) {
		this(layoutId, null);
	}

	public BaseAdapter(int layoutId, OnItemClickListener<T> listener) {
		this(new ArrayList<>(), layoutId, listener);
	}

	public BaseAdapter(List<T> items, int layoutId, OnItemClickListener<T> listener) {
		mItems = items;
		mLayoutId = layoutId;
		mListener = listener;
	}

	public void setOnItemClickListener(OnItemClickListener<T> listener) {
		mListener = listener;
	}

	public void setItems(@NonNull List<T> items) {
		mItems = items;
		notifyDataSetChanged();
	}

	public void addItems(List<T> items) {
		mItems.addAll(items);
		notifyDataSetChanged();
	}

	public void addItem(T item) {
		int position = mItems.size();
		mItems.add(item);
		notifyItemInserted(position);
	}

	public void insertItem(int position, T item) {
		mItems.add(position, item);
		notifyItemInserted(position);
	}

	public void removeItem(int position) {
		mItems.remove(position);
		notifyItemRemoved(position);
	}

	public void updateItem(int position) {
		notifyItemChanged(position);
	}

	public List<T> getItems() {
		return mItems;
	}

	public int getItemPosition(T item) {
		int pos = -1;
		for (int i = 0; i < mItems.size(); i++) {
			if (mItems.get(i).equals(item)) {
				pos = i;
				break;
			}
		}

		return pos;
	}

	public void sortItems(Comparator<T> comparator) {
		Collections.sort(mItems, comparator);
		notifyDataSetChanged();
	}

	@Override
	public VH onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
		return createViewHolder(v);
	}

	@Override
	public void onBindViewHolder(VH holder, int position) {
		holder.bind(mListener, mItems.get(position), position);
	}

	@Override
	public int getItemCount() {
		return mItems.size();
	}

	public interface OnItemClickListener<T> {
		void onItemClick(T item, int position);
	}

	public abstract static class ViewHolder<T, L extends OnItemClickListener> extends RecyclerView.ViewHolder {
		public abstract void bind(L listener, T item, int position);

		public ViewHolder(View itemView) {
			super(itemView);
			ButterKnife.bind(this, itemView);
		}
	}
}
