package me.andrew.footballevents.common.error;


import android.content.Context;
import android.support.v7.app.AlertDialog;

public class ErrorAlertDialog {
	public static void show(Context context, Throwable t) {
		show(context, t.getMessage());
	}

	public static void show(Context context, int messageResId) {
		show(context, context.getResources().getString(messageResId));
	}
	
	public static void show(Context context, String message) {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context)
				.setTitle("Error")
				.setMessage(message)
				.setPositiveButton("Ok", (dialog, which) -> {});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.setCanceledOnTouchOutside(true);
		
		alertDialog.show();
	}
}
