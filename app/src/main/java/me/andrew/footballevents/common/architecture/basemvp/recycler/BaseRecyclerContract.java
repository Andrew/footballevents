package me.andrew.footballevents.common.architecture.basemvp.recycler;

import android.support.annotation.NonNull;

import java.util.Comparator;
import java.util.List;

import me.andrew.footballevents.common.architecture.basemvp.BaseMvpContract;

public interface BaseRecyclerContract {
	interface View<T, P extends Presenter> extends BaseMvpContract.View<P> {
		boolean isDataShown();

		/**
		 * This method calls setItems or addItems according to newList parameter
		 * 
		 * @param items
		 * @param newList
		 */
		void updateItemList(List<T> items, boolean newList);
		
		void setItems(@NonNull List<T> items);
		
		void addItems(List<T> items);
		
		void addItem(T item);

		void insertItem(int position, T item);

		void removeItem(T item);

		void removeItem(int position);

		void sortItems(Comparator<T> comparator);

		void setEmptyViewVisible();

		void setRecyclerVisible();

		void setRefreshing(boolean refreshing);
	}

	interface Presenter extends BaseMvpContract.Presenter {
		void loadItemList(int page, boolean newList);
	}
}