package me.andrew.footballevents.common.architecture.basemvp;

import android.os.Bundle;
import android.support.annotation.Nullable;

import me.andrew.footballevents.common.architecture.base.BaseActivity;
import me.andrew.footballevents.common.error.ErrorAlertDialog;


public abstract class BaseMvpActivity<P extends BaseMvpContract.Presenter> extends BaseActivity
		implements BaseMvpContract.View<P> {

	protected P mPresenter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPresenter = createPresenter();
	}

	@Override
	protected void onStart() {
		super.onStart();
		mPresenter.start();
	}

	@Override
	protected void onStop() {
		mPresenter.stop();
		super.onStop();
	}

	@Override
	public void showError(Throwable t) {
		ErrorAlertDialog.show(this, t);
	}
}
