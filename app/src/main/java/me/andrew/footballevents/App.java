package me.andrew.footballevents;


import android.app.Application;

import me.andrew.footballevents.common.error.UncaughtExceptionHandler;
import me.andrew.footballevents.di.component.AppComponent;
import me.andrew.footballevents.di.component.DaggerAppComponent;
import me.andrew.footballevents.di.module.AppModule;
import me.andrew.footballevents.di.module.CommonModule;
import me.andrew.footballevents.di.module.DataModule;
import me.andrew.footballevents.di.module.KeysModule;
import me.andrew.footballevents.di.module.LinksModule;

public class App extends Application {
	private static AppComponent sComponent;

	public static AppComponent getComponent() {
		return sComponent;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler());
		sComponent = buildComponent();
	}

	private AppComponent buildComponent() {
		return DaggerAppComponent.builder()
				.appModule(new AppModule(getApplicationContext()))
				.commonModule(new CommonModule())
				.dataModule(new DataModule())
				.keysModule(new KeysModule())
				.linksModule(new LinksModule())
				.build();
	}
}
