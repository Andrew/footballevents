
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Standing {

    @SerializedName("_links")
    @Expose
    public StandingLinks links;
    @SerializedName("position")
    @Expose
    public int position;
    @SerializedName("teamName")
    @Expose
    public String teamName;
    @SerializedName("crestURI")
    @Expose
    public String crestURI;
    @SerializedName("playedGames")
    @Expose
    public int playedGames;
    @SerializedName("points")
    @Expose
    public int points;
    @SerializedName("goals")
    @Expose
    public int goals;
    @SerializedName("goalsAgainst")
    @Expose
    public int goalsAgainst;
    @SerializedName("goalDifference")
    @Expose
    public int goalDifference;
    @SerializedName("wins")
    @Expose
    public int wins;
    @SerializedName("draws")
    @Expose
    public int draws;
    @SerializedName("losses")
    @Expose
    public int losses;
    @SerializedName("home")
    @Expose
    public Home home;
    @SerializedName("away")
    @Expose
    public Away away;

}
