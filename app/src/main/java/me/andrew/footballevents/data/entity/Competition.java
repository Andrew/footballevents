
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Competition {

//    @SerializedName("_links")
//    @Expose
//    public Links links;
    @SerializedName("id")
    @Expose
    public int id;
    @SerializedName("caption")
    @Expose
    public String caption;
    @SerializedName("league")
    @Expose
    public String league;
    @SerializedName("year")
    @Expose
    public String year;
    @SerializedName("currentMatchday")
    @Expose
    public int currentMatchday;
    @SerializedName("numberOfMatchdays")
    @Expose
    public int numberOfMatchdays;
    @SerializedName("numberOfTeams")
    @Expose
    public int numberOfTeams;
    @SerializedName("numberOfGames")
    @Expose
    public int numberOfGames;
    @SerializedName("lastUpdated")
    @Expose
    public String lastUpdated;

}
