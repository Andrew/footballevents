package me.andrew.footballevents.data.cache;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.rx_cache2.DynamicKey;
import io.rx_cache2.LifeCache;
import io.rx_cache2.ProviderKey;
import me.andrew.footballevents.data.entity.Competition;
import me.andrew.footballevents.data.entity.LeagueTable;
import me.andrew.footballevents.data.entity.Players;
import me.andrew.footballevents.data.entity.Season;
import me.andrew.footballevents.data.entity.Team;

public interface CacheProviders {
	@ProviderKey("seasonList")
	@LifeCache(duration = 5, timeUnit = TimeUnit.MINUTES)
	Observable<List<Season>> getSeasonList(Observable<List<Season>> o);

	@ProviderKey("competition")
	@LifeCache(duration = 5, timeUnit = TimeUnit.MINUTES)
	Observable<Competition> getCompetition(Observable<Competition> o, DynamicKey id);

	@ProviderKey("leagueTable")
	@LifeCache(duration = 5, timeUnit = TimeUnit.MINUTES)
	Observable<LeagueTable> getLeagueTable(Observable<LeagueTable> o, DynamicKey id);

	@ProviderKey("team")
	@LifeCache(duration = 5, timeUnit = TimeUnit.MINUTES)
	Observable<Team> getTeam(Observable<Team> o, DynamicKey id);

	@ProviderKey("players")
	@LifeCache(duration = 5, timeUnit = TimeUnit.MINUTES)
	Observable<Players> getPlayers(Observable<Players> o, DynamicKey id);
}
