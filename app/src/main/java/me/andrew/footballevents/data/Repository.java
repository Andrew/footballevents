package me.andrew.footballevents.data;

import android.content.Context;

import java.io.File;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.rx_cache2.DynamicKey;
import io.rx_cache2.internal.RxCache;
import io.victoralbertos.jolyglot.GsonSpeaker;
import me.andrew.footballevents.App;
import me.andrew.footballevents.common.network.ConnectionManager;
import me.andrew.footballevents.data.cache.CacheProviders;
import me.andrew.footballevents.data.entity.Competition;
import me.andrew.footballevents.data.entity.LeagueTable;
import me.andrew.footballevents.data.entity.Players;
import me.andrew.footballevents.data.entity.Season;
import me.andrew.footballevents.data.entity.Team;

public class Repository {
	@Inject Context mContext;
	@Inject DataSource.Remote mRemoteDataSource;
	@Inject ConnectionManager mConnectionManager;
	private CacheProviders mCacheProviders;

	public Repository() {
		App.getComponent().inject(this);

		File cacheDir = mContext.getFilesDir();
		mCacheProviders = new RxCache.Builder()
//				.useExpiredDataIfLoaderNotAvailable(true)
				.setMaxMBPersistenceCache(20)
				.persistence(cacheDir, new GsonSpeaker())
				.using(CacheProviders.class);
	}

	public Observable<List<Season>> getSeasonList() {
		return mCacheProviders.getSeasonList(mRemoteDataSource.getSeasonList());
	}

	public Observable<Competition> getCompetition(int id) {
		return mCacheProviders.getCompetition(mRemoteDataSource.getCompetition(id), new DynamicKey(id));
	}

	public Observable<LeagueTable> getLeagueTable(int id) {
		return mCacheProviders.getLeagueTable(mRemoteDataSource.getLeagueTable(id), new DynamicKey(id));
	}

	public Observable<Team> getTeam(String id) {
		return mCacheProviders.getTeam(mRemoteDataSource.getTeam(id), new DynamicKey(id));
	}

	public Observable<Players> getPlayers(String id) {
		return mCacheProviders.getPlayers(mRemoteDataSource.getPlayers(id), new DynamicKey(id));
	}
}
