
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LeagueTable {

//	@SerializedName("_links")
//	@Expose
//	public Links links;
    @SerializedName("leagueCaption")
    @Expose
    public String leagueCaption;
    @SerializedName("matchday")
    @Expose
    public int matchday;
    @SerializedName("standing")
    @Expose
    public List<Standing> standing = null;

}
