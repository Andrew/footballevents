
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Team {

    @SerializedName("_links")
    @Expose
    public TeamLinks links;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("shortName")
    @Expose
    public String shortName;
    @SerializedName("squadMarketValue")
    @Expose
    public Object squadMarketValue;
    @SerializedName("crestUrl")
    @Expose
    public String crestUrl;

}
