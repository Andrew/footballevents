
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlayersLink {

    @SerializedName("href")
    @Expose
    public String href;

}
