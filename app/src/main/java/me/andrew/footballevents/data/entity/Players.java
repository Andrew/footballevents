
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Players {

//    @SerializedName("_links")
//    @Expose
//    public Links links;
    @SerializedName("count")
    @Expose
    public int count;
    @SerializedName("players")
    @Expose
    public List<Player> players = null;

}
