
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TeamLinks {

//    @SerializedName("self")
//    @Expose
//    public Self self;
//    @SerializedName("fixtures")
//    @Expose
//    public Fixtures fixtures;
    @SerializedName("players")
    @Expose
    public PlayersLink playersLink;

}
