package me.andrew.footballevents.data.remote;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observable;
import me.andrew.footballevents.App;
import me.andrew.footballevents.common.network.RetrofitServiceGenerator;
import me.andrew.footballevents.data.DataSource;
import me.andrew.footballevents.data.entity.Competition;
import me.andrew.footballevents.data.entity.LeagueTable;
import me.andrew.footballevents.data.entity.Players;
import me.andrew.footballevents.data.entity.Season;
import me.andrew.footballevents.data.entity.Team;

public class RemoteDataSource implements DataSource.Remote {
	@Inject @Named("base_url") String mBaseUrl;
	@Inject @Named("api_key") String mApiKey;
	private FootballDataService mService;

	public RemoteDataSource() {
		App.getComponent().inject(this);
		mService = RetrofitServiceGenerator.createService(FootballDataService.class, mBaseUrl);
	}
	
	@Override
	public Observable<List<Season>> getSeasonList() {
		return mService.getSeasonList(mApiKey);
	}

	@Override
	public Observable<Competition> getCompetition(int id) {
		return mService.getCompetition(mApiKey, id);
	}

	@Override
	public Observable<LeagueTable> getLeagueTable(int id) {
		return mService.getLeagueTable(mApiKey, id);
	}

	@Override
	public Observable<Team> getTeam(String id) {
		return mService.getTeam(mApiKey, id);
	}

	@Override
	public Observable<Players> getPlayers(String id) {
		return mService.getPlayers(mApiKey, id);
	}
}

