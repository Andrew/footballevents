package me.andrew.footballevents.data;


import java.util.List;

import io.reactivex.Observable;
import me.andrew.footballevents.data.entity.Competition;
import me.andrew.footballevents.data.entity.LeagueTable;
import me.andrew.footballevents.data.entity.Players;
import me.andrew.footballevents.data.entity.Season;
import me.andrew.footballevents.data.entity.Team;

public interface DataSource {
	interface Remote {
		Observable<List<Season>> getSeasonList();

		Observable<Competition> getCompetition(int id);

		Observable<LeagueTable> getLeagueTable(int id);

		Observable<Team> getTeam(String id);

		Observable<Players> getPlayers(String id);
	}
}

