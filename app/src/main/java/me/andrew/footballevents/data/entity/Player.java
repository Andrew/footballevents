
package me.andrew.footballevents.data.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Player {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("position")
    @Expose
    public String position;
    @SerializedName("jerseyNumber")
    @Expose
    public int jerseyNumber;
    @SerializedName("dateOfBirth")
    @Expose
    public String dateOfBirth;
    @SerializedName("nationality")
    @Expose
    public String nationality;
    @SerializedName("contractUntil")
    @Expose
    public String contractUntil;
    @SerializedName("marketValue")
    @Expose
    public Object marketValue;

}
