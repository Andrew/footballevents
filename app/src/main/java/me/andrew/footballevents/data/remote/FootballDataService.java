package me.andrew.footballevents.data.remote;


import java.util.List;

import io.reactivex.Observable;
import me.andrew.footballevents.data.entity.Competition;
import me.andrew.footballevents.data.entity.LeagueTable;
import me.andrew.footballevents.data.entity.Players;
import me.andrew.footballevents.data.entity.Season;
import me.andrew.footballevents.data.entity.Team;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface FootballDataService {

	// there is a typo in test task:
	// use "/competitions/" instead of "/soccerseasons/"
	@GET("/v1/competitions/")
	Observable<List<Season>> getSeasonList(
			@Header("X-Auth-Token") String apiKey);

	@GET("/v1/competitions/{id}")
	Observable<Competition> getCompetition(
			@Header("X-Auth-Token") String apiKey,
			@Path("id") int id);

	@GET("/v1/competitions/{id}/leagueTable")
	Observable<LeagueTable> getLeagueTable(
			@Header("X-Auth-Token") String apiKey,
			@Path("id") int id);

	@GET("/v1/teams/{id}")
	Observable<Team> getTeam(
			@Header("X-Auth-Token") String apiKey,
			@Path("id") String id);

	@GET("/v1/teams/{id}/players")
	Observable<Players> getPlayers(
			@Header("X-Auth-Token") String apiKey,
			@Path("id") String id);
}
