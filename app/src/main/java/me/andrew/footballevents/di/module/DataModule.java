package me.andrew.footballevents.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.andrew.footballevents.data.DataSource;
import me.andrew.footballevents.data.Repository;
import me.andrew.footballevents.data.remote.RemoteDataSource;


@Module
public class DataModule {
	@Provides
	@Singleton
	public DataSource.Remote provideRemoteDataSource() {
		return new RemoteDataSource();
	}
	
	@Provides
	@Singleton
	public Repository provideRepository() {
		return new Repository();
	}
}
