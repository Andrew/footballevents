package me.andrew.footballevents.di.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
	private Context mAppContext;

	public AppModule(Context appContext) {
		mAppContext = appContext;
	}

	@Provides
	@Singleton
	public Context provideAppContext() {
		return mAppContext;
	}
}
