package me.andrew.footballevents.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import me.andrew.footballevents.common.concurrency.AppExecutors;
import me.andrew.footballevents.common.network.ConnectionManager;


@Module
public class CommonModule {
	@Provides
	@Singleton
	public AppExecutors provideAppExecutors() {
		return new AppExecutors();
	}

	@Provides
	@Singleton
	public ConnectionManager provideConnectionManager() {
		return ConnectionManager.getInstance();
	}
}
