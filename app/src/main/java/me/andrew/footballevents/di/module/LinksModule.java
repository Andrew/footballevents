package me.andrew.footballevents.di.module;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class LinksModule {
	@Provides
	@Named("base_url")
	public String provideBaseUrl() {
		return "https://api.football-data.org/";
	}
}
