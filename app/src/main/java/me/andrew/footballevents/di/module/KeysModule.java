package me.andrew.footballevents.di.module;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class KeysModule {
	@Provides
	@Named("api_key")
	public String provideApiKey() {
		return "c1fced87b9624809b2b3b35ac9eed8d5";
	}
}
