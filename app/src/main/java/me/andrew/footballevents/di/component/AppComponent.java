package me.andrew.footballevents.di.component;


import javax.inject.Singleton;

import dagger.Component;
import me.andrew.footballevents.common.network.ConnectionManager;
import me.andrew.footballevents.data.Repository;
import me.andrew.footballevents.data.remote.RemoteDataSource;
import me.andrew.footballevents.di.module.AppModule;
import me.andrew.footballevents.di.module.CommonModule;
import me.andrew.footballevents.di.module.DataModule;
import me.andrew.footballevents.di.module.KeysModule;
import me.andrew.footballevents.di.module.LinksModule;
import me.andrew.footballevents.presentation.competition.CompetitionPresenter;
import me.andrew.footballevents.presentation.leaguetable.recycler.LeagueTablePresenter;
import me.andrew.footballevents.presentation.players.recycler.PlayersPresenter;
import me.andrew.footballevents.presentation.seasons.recycler.SeasonsPresenter;


@Singleton
@Component(modules = {AppModule.class, CommonModule.class, DataModule.class, KeysModule.class, LinksModule.class})
public interface AppComponent {
	// common
	void inject(ConnectionManager o);

	//	data
	void inject(Repository o);
	void inject(RemoteDataSource o);
	
	// presenter
	void inject(SeasonsPresenter o);
	void inject(CompetitionPresenter o);
	void inject(LeagueTablePresenter o);
	void inject(PlayersPresenter o);
}
